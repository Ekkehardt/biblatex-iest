## biblatex-iest: Changelog
###### E. Frank Sandig -- 2017-03-06
- - -
### 1.02 - 2016-03-10
- firstinits -> giveninits (since biblatex 3.3)
- Will NOT work with earlier Versions of biblatex

### 1.01 - 2015-11-04
- CR+LF line ends to make the code readable for Windows users
- Still works in TeX Live 2016 on 1 Mar 2017

### 1.0 - 2015-09-01
- First version to fulfill all requirements properly

### * Plan for 1.10
Scheduled for 2017-04
- [x] Based on 1.02 (no branch so far)
- [x] Additional 'draft' variant for standalone lists
- [x] English comments in style files
- [x] Move Repo to gitlab.com
- [x] Readme --> EN
- [x] Changelog --> EN
- [x] Example tex files --> EN
- [x] Example bib file --> EN
- [x] the style and doc repos in one, rename dok to doc in EN
- [x] highlight `note` in draft variant
- [ ] Update dok_biblatex-iest_DE (listings!) [1]
- [ ] Translate documentation to doc_biblatex-iest_EN [1]
- [ ] New screenshots

[1]: In Progress
- - -
### WISHLIST
- Check and add options for more languages, maybe in a .lbx file
