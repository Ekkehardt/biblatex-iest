## biblatex-iest
- - -

### Bibliography style for the 'IEST'

The `iest` style in general behaves like the `numeric-comp` standard style, which it imports. Yet it deviates in regard of the following features:

1. Prints names of authors, editors and translators in the order 'last name, given name(s)'
2. Uses initials for given names throughout
3. Shortens the author list with 'et al.' instead of 'u.a.' for more than three names, even when `ngerman` is selected as a `babel` package option
4. Closes the list of authors with a colon ':'
5. Formats the publisher information as 'publisher, location, date'
6. Sorts the bibliography according to the order of citations in the running text
7. Prints references as ragged paragraphs throughout the bibliography

With this bibliography style, requirements for theses at the Institute of Iron and Steel Technology at TU Bergakademie Freiberg (IIST, German: IEST) are being implemented.

For documenting the bibliography style `iest`, an article has been published at http://texwelt.de/blog/modifizieren-eines-biblatex-stils/ (German) during the early development phase. It has been updated several times to meet changes made in `iest`.

In August 2015 a further updated version of this article has been published by the web journal 'freiesMagazin', which has been discontinued by the end of the year 2016. The issue containing that article can be found at http://www.freiesmagazin.de/ftp/2015/freiesMagazin-2015-08.pdf (German). This version has been published again at http://www.pro-linux.de/artikel/2/1787/modifizieren-eines-biblatex-stils.html on 27 Aug 2015 (German). By that time, Johannes Böttcher and moewew started contributing to this work.

A PDF version of the article, optimized for paper, is being provided at http://gitlab.com/Ekkehardt/dok-biblatex-iest. This version is kept up-to-date with the releases of the `iest` style, and can be regarded to be the official documentation. With the release of version 1.10, scheduled for spring 2017, an English version is also going to be provided, and all documentation shall be available this style repository. The doc/dok-PDFs here are work-in-progress.

From 1.10 on, a draft variant named `iest-draft` is going to be provided along with the `iest` style. Using it, the reference information for each publication is intended to look exactly like with `iest`, but with printing citation keys instead of reference numbers, just like the `draft` standard style does, which it is based on. Additionally, the sorting order is set to be 'name-year-volume-title' (instead of sorting the list in citation order) for the draft variant. The primary purpose of this variant is creating standalone (categorized or filtered) reference lists for reviews and discussions, rather than application in actual theses.

- - -

### Installation in TeX Live

Copy the files <tt>iest.cbx</tt>, <tt>iest.bbx</tt>, <tt>iest-draft.cbx</tt> and <tt>iest-draft.bbx</tt> to <tt>~/texmf/tex/latex/biblatex-iest/</tt>.

If not yet existing, the directory tree has to be created and made user readable.

### Installation in MacTeX

Copy the files <tt>iest.cbx</tt>, <tt>iest.bbx</tt>, <tt>iest-draft.cbx</tt> and <tt>iest-draft.bbx</tt> to <tt>~/Library/texmf/tex/latex/biblatex-iest/</tt>.

If not yet existing, the directory tree has to be created and made user readable.

### Installation in MikTeX

1. Create a private tree, e.g. under <tt>~/texmf</tt> (has to be outside of the MikTeX install path)

2. Copy the files <tt>iest.cbx</tt>, <tt>iest.bbx</tt>, <tt>iest-draft.cbx</tt> and <tt>iest-draft.bbx</tt> to <tt>~/texmf/tex/latex/biblatex-iest/</tt>.

3. Register the private tree as 'Root':
	1. MikTeX Settings
	2. Category 'Roots'
	3. Click 'Add..'
	4. Select directory named like given above
	5. Change search order, if necessary
	6. Click 'Apply'

- - -

### Platforms

Independent

Tested with TeX Live 2014, 2015 and 2016 in Ubuntu 14.04 LTS and 16.04 LTS along with the `tubaf-base` package (Corporate Design of TU Bergakademie Freiberg in LaTeX classes and styles), as well as `scrbook` and `scrartcl`.

### References

Apart from its use at the IIST in Freiberg/SN, the `iest` bibliography style has been used for the proceedings of the XXII. International Students' Day of Metallurgy (ISDM) 2015, which has taken place in May 2015 at RWTH Aachen University.

Since publication of the articles named above, the style has been referred to multiple times in LaTeX forums and discussion groups among beginners and intermediate users of biblatex, as well as style creators.

### License

The `iest` style, including the draft variant, is licensed under the terms of **LPPL 1.3**.

- - -

E. Frank Sandig
2017-03-06
